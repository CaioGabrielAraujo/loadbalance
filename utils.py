
class LimitException(Exception):
    pass

class FileException(Exception):
    pass


def file_manipulating():
    try:
        file_reader = open('input.txt', 'r')
        file_writer = open('output.txt', 'w')
    except FileException:
        print("Erro na leitura ou criação dos arquivos")
    return file_reader, file_writer

def check_ttask(User):
    if User.ttask < 1 or User.ttask > 10:
        raise LimitException('ttask Inválido')
    return True

def check_umax(server):
    if server.umax < 1 or server.umax > 10:
        raise LimitException('umax Inválido')
    return True

def read_next_line(file_reader):
    linha = file_reader.readline()
    if linha: 
        return int(linha)
    else:
        return False

def commom_data_output(file_writer, servers):
    output = ''
    for i, server in enumerate(servers):
        if i + 1 == len(servers):
            output += '%s\n' % len(server.users)
        else:
            output += '%s,' % len(server.users)
    file_writer.write(output)
    return output

