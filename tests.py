import os
import unittest
import random
from balance import LoadBalance
from models import Server, User
from utils import *


class UnitTests(unittest.TestCase):

    def setUp(self):
        self.reader = open('test_input.txt', 'r')
        User.ttask = int(self.reader .readline())
        Server.umax = int(self.reader .readline())
        self.load_balance = LoadBalance(open('test_input.txt', 'r'), open('test_output.txt', 'w'))

    def test_file_reader_write(self):
        self.assertIsNotNone(file_manipulating())
    
    def test_check_ttask(self):
        self.assertEqual(check_ttask(User), True)

    def test_check_space(self):
        User.ttask = 4
        Server.umax = 3
        user = [User(), User()]
        server = Server(user)
        self.assertEqual(server.check_space(), True)

    def test_remove_tick(self):
        User.ttask = 4
        user = User()
        self.assertEqual(user.remove_tick(), 3)

    def test_check_umax(self):
        self.assertEqual(check_umax(Server), True)

    def test_read_line(self):
        self.assertEqual(read_next_line(self.reader), 1)

    def test_limit_exception(self):
        User.ttask = 11
        with self.assertRaises(LimitException):
         check_ttask(User)
        
    def test_active_users_len(self):
        self.assertGreaterEqual(self.load_balance.active_users_len(), 0)




if __name__ == '__main__':
    unittest.main()        