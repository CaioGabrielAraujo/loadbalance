
from models import Server, User
from utils import commom_data_output, read_next_line

class LoadBalance:

    def __init__(self, file_reader, file_writer):
        self.file_reader = file_reader 
        self.file_writer = file_writer  
        self.server_cost = 0
        self.servers = []

    def expire_tick_caller(self):
        self.servers = [server for server in self.servers if server.expire_user_tick()]
            
    def active_users_len(self):
        len = 0
        for server in self.servers:
            len += len(server.users)
        return len

    def append_new_users(self, user_len):
        iterations = range(user_len)
        for i in iterations:
            alocado = False
            for server in self.servers:
                if server.check_space():
                    server.users.append(User())
                    alocado = True
            if alocado is False:
                self.servers.append(Server([User()]))

    def init(self):
        read_line = False
        while self.servers or not read_line:
            new_users_len = read_next_line(self.file_reader)
            if new_users_len is False and read_line is False:
                read_line = True

            if new_users_len is not False:
                self.append_new_users(new_users_len)

            commom_data_output(self.file_writer, self.servers)
            self.server_cost += len(self.servers)
            self.expire_tick_caller()
    
    #Final Results
        self.file_writer.write(str(self.active_users_len()) + '\n')  
        self.file_writer.write(str(self.server_cost))  