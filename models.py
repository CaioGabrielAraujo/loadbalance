from utils import LimitException

class User:
    ttask = 0

    def __init__(self):
        self.tick_amount = User.ttask

    def remove_tick(self):
        self.tick_amount -= 1
        if self.tick_amount < 0:
            raise LimitException("Um erro ocorreu, ttask restantes menor que 0")
        return self.tick_amount


class Server:
    umax = 0

    def __init__(self, users):
        self.users = users

    def check_space(self):
        if len(self.users) < Server.umax:
            return True
        else:
            return False

    def expire_user_tick(self):
        self.users = [user for user in self.users if user.remove_tick()]
        return self.users
