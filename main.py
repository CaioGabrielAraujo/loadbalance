from balance import LoadBalance
from models import User, Server
from utils import check_ttask, check_umax, file_manipulating

def main():
    
    file_reader, file_writer = file_manipulating()

    load_balance = LoadBalance(file_reader, file_writer)

    User.ttask = int(file_reader.readline())
    Server.umax = int(file_reader.readline())

    check_ttask(User)
    check_umax(Server)

    load_balance.init()
        
main()